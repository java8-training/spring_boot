package com.encaps.emp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
//@EnableRetry

public class EmpApplication {

	public static void main(String[] args) {
	SpringApplication.run(EmpApplication.class, args);
	//RetryServiceImpl retryServiceImpl=applicationContext.getBean(RetryServiceImpl.class);
	//retryServiceImpl.retry();
	}
	
}
