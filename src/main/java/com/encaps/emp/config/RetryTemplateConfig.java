package com.encaps.emp.config;



import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class RetryTemplateConfig {
	@Value("${retry.maxAttempts}")
	private int maximumAttempts;
	
	@Value("${retry.backOffDelay}")
	private int backOffDelay;
	@Bean

	 public RetryTemplate retryTemplate() {

	     SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();

	     retryPolicy.setMaxAttempts(maximumAttempts);


	     FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();

	     backOffPolicy.setBackOffPeriod(backOffDelay);


	     RetryTemplate template = new RetryTemplate();

	     template.setRetryPolicy(retryPolicy);

	     template.setBackOffPolicy(backOffPolicy);


	     return template;

	    }
}
