package com.encaps.emp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.encaps.emp.model.AccountDetails;
import com.encaps.emp.search.SearchCriteria;
import com.encaps.emp.service.impl.AccountDetailsServiceImpl;


@RestController
@RequestMapping("/account")
public class AccountDetailsController {
	@Autowired
	AccountDetailsServiceImpl accountDetailsService;
	
	
	@PostMapping("/searchbycriteria")
	public List<AccountDetails> search(@RequestBody SearchCriteria searchCriteria){
		return accountDetailsService.search(searchCriteria);
	}
	@GetMapping("/test")
	public ResponseEntity<String> test() {
		
	    return ResponseEntity.status(201)
	    	.header("custom header", "example value")
	        .body("Custom header set");
	    
	    
	}
	
	@GetMapping("/customHeader")
	ResponseEntity<String> customHeader() {
		HttpHeaders headers=new HttpHeaders();
		headers.add("Custom-Header1", "value1 ");
		headers.add("Custom-Header2", "value2");
		
	    return ResponseEntity.status(HttpStatus.NO_CONTENT)
	    	.headers(headers)
	        .body("Custom header set");
	    
	    
	}
	@GetMapping("/{id}")
	public ResponseEntity<AccountDetails> findById(@PathVariable Integer id) 
	{
		Optional<AccountDetails> accountDetails=accountDetailsService.findById(id);
		if(accountDetails.isPresent())
			return ResponseEntity.ok(accountDetails.get());
		else
			return ResponseEntity.status(210).build();
	}
	
	
	@GetMapping("/get/{id}")
	public AccountDetails findById2(@PathVariable Integer id) 
	{
		Optional<AccountDetails> accountDetails=accountDetailsService.findById(id);
		if(accountDetails.isPresent())
			return accountDetails.get();
		else
			throw new ResponseStatusException(
			          HttpStatus.BAD_REQUEST, "Provide correct Actor Id", new RuntimeException());
			   
	}
	
}
	
