package com.encaps.emp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.encaps.emp.exception.RecordNotFoundException;
import com.encaps.emp.model.Employee;
import com.encaps.emp.model.EmployeeDetails;
import com.encaps.emp.search.SearchCriteria;
import com.encaps.emp.service.EmployeeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(tags="controller of employee")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping("/")
	@ApiOperation(value = "returns a message")
	public String getMessage() {
		return "hello";
	}
	
	@ApiOperation(value = "returns details of all employee")
	@GetMapping("/getall")
	public List<Employee> getAllEmployees() {
		return employeeService.findAll();
	}
	@GetMapping("/getbyname")
	public List<Employee> getAllEmployeesByName(@ApiParam(
			value="name of the employee")String name) {
		return employeeService.findByNameIgnoreCase(name);
	}
	@GetMapping("/getbynameage")
	public List<Employee> getAllEmployeesByNameAge(String name,Integer age) {
		return employeeService.findByNameAge(name,age);
	}
	@PostMapping("/create")
	public Employee createEmployee(@RequestBody Employee employee) {
		return employeeService.create(employee);
	}
	
	@PostMapping("/search")
	public List<Employee> search(@RequestBody Employee employee) {
		return employeeService.search(employee);
	}

	@GetMapping("/updatesalary")
	public int updateSalary() {
		return employeeService. updateSalary();
	}
	
	@GetMapping("/deletebyname")
	public int deleteByName(String name) {
		return employeeService. deleteByName( name);
	}
	@GetMapping("/findbygreaterthan")
	public List<Employee> findAllGreaterThan(String key,Integer value,String operation) {
		return employeeService.findAllGreaterThan(key,value,operation);
	}
	
	
	@GetMapping("/getalldetails")
	public List<EmployeeDetails> findAllEmployeeDetails() {
		return employeeService.findAllEmployeeDetails();
		
	}
	@GetMapping("/getalldetailsbyname")
	public List<EmployeeDetails> findAllEmployeeDetailsByName(String name) {
		return employeeService.findAllEmployeeDetailsByName(name);
	}
	@PostMapping("/searchbycriteria")
	public List<Employee> search(@RequestBody SearchCriteria searchCriteria){
		return employeeService.search(searchCriteria);
	}
	
	@DeleteMapping("/delete/{id}")
	public void  deleteEmployee(@PathVariable int id){
		 employeeService.deleteById(id);
	}
	@PutMapping("/update/{id}")
	public Employee updateEmployee(@PathVariable int id,@RequestBody Employee employee) throws RecordNotFoundException {
		
			return employeeService.update(id,employee);
		
	
	}
	
	
}
