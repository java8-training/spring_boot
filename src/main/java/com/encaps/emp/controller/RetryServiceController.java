package com.encaps.emp.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.encaps.emp.service.impl.RetryServiceImpl;

@RestController
@RequestMapping("/retry")
public class RetryServiceController {
	@Autowired
	RetryServiceImpl retryService;

	@GetMapping("/test1")
	public void customHeader1() {
		retryService.retry();

	}
	@GetMapping("/test2")
	public void customHeader2() {
		retryService.retry();
    
	}
	/*@ExceptionHandler({ ArithmeticException.class })
	public ResponseEntity<Object> handleException(ArithmeticException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	}
	@ExceptionHandler({ SQLException.class })
	public ResponseEntity<Object> handleException2(ArithmeticException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	}*/
}
