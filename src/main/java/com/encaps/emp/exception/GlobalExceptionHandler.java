package com.encaps.emp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

	 @ExceptionHandler(ArithmeticException.class)
	 public ResponseEntity<Object> handleException(ArithmeticException e) {
		 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
	 }
	 
	 @ExceptionHandler(RecordNotFoundException.class)
	 public ResponseEntity<Object> handleRecordNotFoundException(RecordNotFoundException e) {
		 return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
	 }
}
