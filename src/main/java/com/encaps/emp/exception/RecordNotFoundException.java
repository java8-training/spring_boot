package com.encaps.emp.exception;

import lombok.Value;

@Value

public class RecordNotFoundException extends Exception {
	String errorMessage;

	public String getMessage() {
		return errorMessage;
	}
}
