package com.encaps.emp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="account_details")
@Getter
@Setter
public class AccountDetails {
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Integer id;	
	Long accountNumber;
	String name;
	Double balance;
	public AccountDetails() {}
	public AccountDetails(Long accountNumber, String name, Double balance) {
		super();
		this.accountNumber = accountNumber;
		this.name = name;
		this.balance = balance;
	}

}
