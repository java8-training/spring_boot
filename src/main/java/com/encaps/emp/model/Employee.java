package com.encaps.emp.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Entity
@Table(name="emp")
@Data
@EntityListeners(AuditingEntityListener.class)
public class Employee {
@Id	
@GeneratedValue(strategy = GenerationType.IDENTITY)
Integer id;	
@ApiModelProperty(notes = "Name of the employee")
String name;
@ApiModelProperty(notes = "Age of the employee")
Integer age;
@ApiModelProperty(notes = "salary of the employee")
Double salary;
@CreatedDate
LocalDateTime createdDate;
@LastModifiedDate
LocalDateTime lastModifiedDate;
public Employee() {

}
public Employee(String name, int age, double salary) {
	super();
	this.name = name;
	this.age = age;
	this.salary = salary;
}


}
