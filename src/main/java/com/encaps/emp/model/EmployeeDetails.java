package com.encaps.emp.model;

import org.springframework.beans.factory.annotation.Value;

public interface EmployeeDetails {
	String getName();
	Integer getAge();
	Double getSalary();
    default double getHRA()
     {
    	return  getSalary()*0.1;
     }
    default double getDA()
    {
   	return  getSalary()*0.15;
    }
    @Value("#{target.salary*0.1}")
    Double getCCA();
    
    String getCompany();
    String getAddress();
}
