package com.encaps.emp.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeSalaryDetails {
	String name;
	int age;
	Double salary;
	Double incomeTax;
	public EmployeeSalaryDetails(String name, int age, Double salary) {
		super();
		this.name = name;
		this.age = age;
		this.salary = salary;
	}
	
}
