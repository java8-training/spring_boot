package com.encaps.emp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
@Data
public class University {
String name;
String country;
@JsonProperty("alpha_two_code")
String alphaTwoCode;
@JsonProperty("state-province")
String stateProvince;
List<String>domains;
@JsonProperty("web_pages")
List<String>webPages;

}
