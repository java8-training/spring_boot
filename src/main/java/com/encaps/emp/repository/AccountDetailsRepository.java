package com.encaps.emp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.encaps.emp.model.AccountDetails;
@Repository
public interface AccountDetailsRepository extends JpaRepository<AccountDetails, Integer>,JpaSpecificationExecutor<AccountDetails> {

}
