package com.encaps.emp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.encaps.emp.model.Employee;
import com.encaps.emp.model.EmployeeDetails;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Integer> ,
JpaSpecificationExecutor<Employee>{
	List<Employee> findByNameIgnoreCase(String name);
	//@Query(value="select * from emp  where name=:name and age=:age",nativeQuery=true)
	List<Employee> findByNameAndAge(String name,Integer age);
	
	@Modifying
	@Transactional
	@Query(value="update Employee set salary=salary+1000")
	int updateSalary();
	
	@Modifying
	@Transactional
	@Query(value="delete from  Employee where name =:name")
	int deleteByName(String name);
	
	//@Query(value="select e from Employee e")
	List<EmployeeDetails> findByName(String name);
	
	    @Query(value="select emp.*,employer.name as company,employer.address from emp inner join employer on emp.employer_id=employer.id",nativeQuery=true)
		List<EmployeeDetails> findAllEmployeeDetails();
	    
	  
}
