package com.encaps.emp.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.encaps.emp.model.Employee;

public class EmployeeSpecification<T> implements Specification<T>{
	String key;
	Integer value;
	String operation;
	
	public EmployeeSpecification(String key, Integer value, String operation) {
		super();
		this.key = key;
		this.value = value;
		this.operation = operation;
	}

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		if(operation.equals("greater"))
			 return criteriaBuilder.greaterThan(root.get(key), value);
				else
				return 	 criteriaBuilder.lessThan(root.get(key), value);
	}

}
