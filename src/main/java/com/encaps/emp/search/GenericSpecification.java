package com.encaps.emp.search;



import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class GenericSpecification<T> implements Specification<T>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	SearchCriteria searchCriteria;
	
	

	public GenericSpecification(SearchCriteria searchCriteria) {
		super();
		this.searchCriteria = searchCriteria;
	}



	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate predicate=null;
		switch(searchCriteria.getOperation())
		{
		case EQUAL:predicate=criteriaBuilder.equal(criteriaBuilder.lower(root.get(searchCriteria.getField())), searchCriteria.getValue().toString().toLowerCase());
			break;
		case GREATER_THAN:predicate=criteriaBuilder.greaterThan(root.get(searchCriteria.getField()), searchCriteria.getValue().toString());
			break;
		case GREATER_THAN_EQUAL:predicate=criteriaBuilder.greaterThanOrEqualTo(root.get(searchCriteria.getField()), searchCriteria.getValue().toString());
			break;
		case LESS_THAN:predicate=criteriaBuilder.lessThan(root.get(searchCriteria.getField()), searchCriteria.getValue().toString());
			break;
		case LESS_THAN_EQUAL:predicate=criteriaBuilder.lessThanOrEqualTo(root.get(searchCriteria.getField()), searchCriteria.getValue().toString());
			break;
		case NOT_EQUAL:predicate=criteriaBuilder.notEqual(root.get(searchCriteria.getField()), searchCriteria.getValue().toString());
			break;
		
			
		}
	
		
		return predicate;
	}

}
