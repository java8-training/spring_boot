package com.encaps.emp.search;

import lombok.Value;

@Value
public class SearchCriteria {
		 String field;
		 String type;
	     Object value;
	     SearchOperation operation;
}
