package com.encaps.emp.service;

import java.util.List;

import com.encaps.emp.exception.RecordNotFoundException;
import com.encaps.emp.model.Employee;
import com.encaps.emp.model.EmployeeDetails;
import com.encaps.emp.search.SearchCriteria;

public interface EmployeeService {

	List<Employee> findAll();

	List<Employee> findByNameIgnoreCase(String name);

	List<Employee> findByNameAge(String name, Integer age);

	Employee create(Employee employee);

	List<Employee> search(Employee employee);
	List<Employee> search(SearchCriteria searchCriteria);
	//List<EmployeeDetails> findAllEmployeeDetails();
	int updateSalary();
	
	int deleteByName(String name);
	public  List<Employee> findAllGreaterThan(String key,Integer value,String operation) ;
	
	List<EmployeeDetails> findAllEmployeeDetailsByName(String name);
	List<EmployeeDetails> findAllEmployeeDetails();
	void deleteById(Integer id);
	Employee update(Integer id,Employee employee)throws RecordNotFoundException ;


}
