package com.encaps.emp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.encaps.emp.model.AccountDetails;
import com.encaps.emp.repository.AccountDetailsRepository;
import com.encaps.emp.search.GenericSpecification;
import com.encaps.emp.search.SearchCriteria;
@Service
public class AccountDetailsServiceImpl {
	@Autowired
	AccountDetailsRepository accountDetailsRepository;

	public List<AccountDetails> search(SearchCriteria searchCriteria) {
		return accountDetailsRepository.findAll(new GenericSpecification<AccountDetails>(searchCriteria));
	}
	
	public Optional<AccountDetails> findById(Integer id) {
		return accountDetailsRepository.findById(id);
	}
}
