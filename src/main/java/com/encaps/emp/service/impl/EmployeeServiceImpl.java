package com.encaps.emp.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.encaps.emp.exception.RecordNotFoundException;
import com.encaps.emp.model.Employee;
import com.encaps.emp.model.EmployeeDetails;
import com.encaps.emp.repository.EmployeeRepository;
import com.encaps.emp.repository.EmployeeSpecification;
import com.encaps.emp.search.GenericSpecification;
import com.encaps.emp.search.SearchCriteria;
import com.encaps.emp.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;
	// private static final Logger log =
	//Logger log=LoggerFactory.getLogger(EmployeeServiceImpl.class);

	public List<Employee> findAll() {
		return employeeRepository.findAll();
	}

	@Override
	public List<Employee> findByNameIgnoreCase(String name) {
		// TODO Auto-generated method stub
		return employeeRepository.findByNameIgnoreCase(name);
	}

	@Override
	public List<Employee> findByNameAge(String name, Integer age) {
		// TODO Auto-generated method stub
		return employeeRepository.findByNameAndAge(name, age);
	}

	@Override
	public Employee create(Employee employee) {
		// TODO Auto-generated method stub
		return employeeRepository.save(employee);
	}

	@Override
	public List<Employee> search(Employee employee) {

		return employeeRepository.findAll(Example.of(employee));
	}

	@Override
	public int updateSalary() {
		// TODO Auto-generated method stub
		int count = employeeRepository.updateSalary();
		if (true)
			throw new RuntimeException();
		return count;
	}

	@Override
	public int deleteByName(String name) {
		// TODO Auto-generated method stub
		return employeeRepository.deleteByName(name);
	}

	/*
	 * public List<EmployeeDetails> findAllEmployeeDetails() { // TODO
	 * Auto-generated method stub return employeeRepository.findBySalaryNotNull(); }
	 */

	public List<Employee> findAllGreaterThan(String key, Integer value, String operation) {
		/*
		 * return employeeRepository.findAll((root,quer,criteriaBuilder)->{
		 * if(operation.equals("greater")) return
		 * criteriaBuilder.greaterThan(root.get(key), value); else return
		 * criteriaBuilder.lessThan(root.get(key), value); });
		 */

		return employeeRepository.findAll(new EmployeeSpecification(key, value, operation));
	}

	@Override
	public List<Employee> search(SearchCriteria searchCriteria) {
		log.info("entrering search() method ");
		return employeeRepository.findAll(new GenericSpecification<Employee>(searchCriteria));

	}

	@Override
	public List<EmployeeDetails> findAllEmployeeDetails() {

		return employeeRepository.findAllEmployeeDetails();
	}

	@Override
	public List<EmployeeDetails> findAllEmployeeDetailsByName(String name) {
		// TODO Auto-generated method stub
		return employeeRepository.findByName(name);
	}

	@Override
	public void deleteById(Integer id) {
		//employeeRepository.deleteById(id);

		if (employeeRepository.existsById(id))
			employeeRepository.deleteById(id);
		else
			throw new RuntimeException("record doesnot exists");

	}

	@Override
	public Employee update(Integer id, Employee employee) throws RecordNotFoundException {
		Optional<Employee> existingEmployee = employeeRepository.findById(id);
		if (existingEmployee.isPresent()) {
			employee.setId(id);
			employee.setCreatedDate(existingEmployee.get().getCreatedDate());
			return employeeRepository.save(employee);

		} else
			throw new RecordNotFoundException("record doesnot exists");
	}

}
