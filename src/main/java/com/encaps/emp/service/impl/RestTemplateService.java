package com.encaps.emp.service.impl;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.encaps.emp.model.Employee;
import com.encaps.emp.model.University;

@Component
public class RestTemplateService {
	//@Scheduled(fixedDelay = 1000 * 60 * 60)
	void retrieveUniversities() {
		RestTemplate restTemplate = new RestTemplate();
		University[] universities = restTemplate.getForObject("http://universities.hipolabs.com/search?country=india",
				University[].class);
		for (University university : universities) {
			System.out.println(university);
		}
	}

	//@Scheduled(fixedDelay = 1000 * 60 * 60)
	void postEmployee() {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpEntity<Employee> request = new HttpEntity<>(new Employee("sajin",25,30000));
		
		//Employee employee = restTemplate.postForObject("http://localhost:8080/create", request, Employee.class);
		
		Employee employee = restTemplate.exchange("http://localhost:8080/update/13", HttpMethod.PUT,request, Employee.class).getBody();
		System.out.println(employee);
		
	}
}
