package com.encaps.emp.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@EnableRetry
public class RetryServiceImpl {
	@Autowired 
	RetryTemplate retryTemplate;
 @Retryable(value= {ArithmeticException.class},maxAttemptsExpression="${retry.maxAttempts}",backoff= @Backoff(delayExpression = "${retry.backOffDelay}",multiplier=2))	
 public void retry() 
 {
	 log.info("entering retry() -{}",LocalDateTime.now());
	int a=10/0;
	 log.info("exiting retry()");
 }
 //@Recover
 void recover(ArithmeticException e)
 {
	 log.info("entering recover()"+e.getMessage());
 }
 
	public void retryUsingRetryTemplate()  
	{
			retryTemplate.execute(

			     context -> 
			     {
			     log.info("entering retry() -{}",LocalDateTime.now());
			 	 int a=10/0;
			 	 log.info("exiting retry()");
			 	 return true;
			     });  
	}
}
