package com.encaps.emp.service.impl;

import java.time.LocalDateTime;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;
@EnableScheduling
@Component

//@EnableAsync
public class Scheduler {
//@Scheduled(cron = "${rate}")
void reader() {
	System.out.println("reader statrted"+LocalDateTime.now());
}
//@Async
//@Scheduled(initialDelay=1000, fixedDelay=100)
public void testScheduling() throws InterruptedException {
    System.out.println("Started : "+LocalDateTime.now()+""+ Thread.currentThread().getName());
    Thread.sleep(4000);
    System.out.println("Finished : "+ Thread.currentThread().getName());
}
}
