package com.encaps.emp.service.impl;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import com.encaps.emp.model.EmployeeSalaryDetails;
@EnableScheduling
@Component()
public class SpelEvaluatorServiceImpl {


//@Scheduled(fixedDelay=1000*60*60)	
void evaluate()
{
	
	
	 
	 /*ExpressionParser expressionParser = new SpelExpressionParser();
	 Expression expression = expressionParser.parseExpression("10+2.5");
	 System.out.println( expression.getValue());
	  expression = expressionParser.parseExpression("10<2.5");
	 System.out.println( expression.getValue());
	 expression = expressionParser.parseExpression("10<20 and 30>10");
	 System.out.println( expression.getValue());
*/
	ExpressionParser expressionParser = new SpelExpressionParser();
	EmployeeSalaryDetails employeeSalaryDetails=new EmployeeSalaryDetails("vipin",25,40000.0);
		
	 Expression expression = expressionParser.parseExpression("salary*12>250000?salary*0.1:salary*0.2");
	 StandardEvaluationContext evaluationContext = new StandardEvaluationContext(employeeSalaryDetails);
     System.out.println(expression.getValue(evaluationContext));
     expressionParser.parseExpression("incomeTax").setValue(evaluationContext,expression.getValue(evaluationContext));
     System.out.println(employeeSalaryDetails.getIncomeTax());
   
}
}
